require 'test_helper'

class Yandex::CampaignsControllerTest < ActionController::TestCase
  setup do
    @yandex_campaign = yandex_campaigns(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:yandex_campaigns)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create yandex_campaign" do
    assert_difference('Yandex::Campaign.count') do
      post :create, yandex_campaign: { email_notifications_id: @yandex_campaign.email_notifications_id, strategy_id: @yandex_campaign.strategy_id }
    end

    assert_redirected_to yandex_campaign_path(assigns(:yandex_campaign))
  end

  test "should show yandex_campaign" do
    get :show, id: @yandex_campaign
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @yandex_campaign
    assert_response :success
  end

  test "should update yandex_campaign" do
    patch :update, id: @yandex_campaign, yandex_campaign: { email_notifications_id: @yandex_campaign.email_notifications_id, strategy_id: @yandex_campaign.strategy_id }
    assert_redirected_to yandex_campaign_path(assigns(:yandex_campaign))
  end

  test "should destroy yandex_campaign" do
    assert_difference('Yandex::Campaign.count', -1) do
      delete :destroy, id: @yandex_campaign
    end

    assert_redirected_to yandex_campaigns_path
  end
end
