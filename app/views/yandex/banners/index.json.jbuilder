json.array!(@yandex_banners) do |yandex_banner|
  json.extract! yandex_banner, :id, :banner_id, :campaign_id, :title, :text, :href, :geo
  json.url yandex_banner_url(yandex_banner, format: :json)
end
