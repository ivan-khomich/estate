json.array!(@yandex_campaigns) do |yandex_campaign|
  json.extract! yandex_campaign, :id, :strategy_id, :email_notifications_id
  json.url yandex_campaign_url(yandex_campaign, format: :json)
end
