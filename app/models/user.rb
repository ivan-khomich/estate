class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :campaigns, class_name: "Yandex::Campaign", :foreign_key => :campaign_id
  
  def admin?
    self.role > 0
  end
end
