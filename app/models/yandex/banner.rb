class Yandex::Banner < ActiveRecord::Base
  belongs_to :campaign, class_name: "Yandex::Campaign", :foreign_key => :campaign_id
  serialize :phrases, Array

  def create
    Yandex::API::Direct::request('CreateOrUpdateBanners', self.to_hash)
  end

  def to_hash
    [{"BannerID" => self.banner_id, 
     "CampaignID" => self.campaign.campaign_id,
     "Title" => self.title,
     "Text" => self.text,
     "Href" => self.href,
     "Geo" => self.geo,
     "Phrases" => Array(self.phrases.map{|p| {"Phrase" => p, "Price" => '1.1'}})
   }]
  end
end
