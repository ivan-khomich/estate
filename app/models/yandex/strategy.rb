class Yandex::Strategy < ActiveRecord::Base
  StrategyNames = { "HighestPosition" => "Наивысшая доступная позиция" , 
    "LowestCost" => "Показ в блоке по минимальной цене", 
    "LowestCostPremium" => "Показ в блоке по минимальной цене",
    "WeeklyBudget" => "Недельный бюджет",
    "WeeklyPacketOfClicks" => "Недельный пакет кликов",
    "AverageClickPrice" => "Средняя цена клика",
    "NoPremiumPosition" => "Показ под результатами поиска",
    "IndependentControl" => "Независимое управление для разных типов площадок"}

  belongs_to :campaign, class_name: "Yandex::Campaign", :foreign_key => :campaign_id


  def to_hash
    {"StrategyName" => self.name,
     # "WarnPlaceInterval" => self.WarnPlaceInterval,
     # "MoneyWarningValue" => self.MoneyWarningValue}
   }
  end

end
