class Yandex::EmailNotificationInfo < ActiveRecord::Base

  belongs_to :campaign, class_name: "Yandex::Campaign", :foreign_key => :campaign_id

  def to_hash
    {"Email" => self.campaign.user.email,
     "MoneyWarningValue" => 2,
     "WarnPlaceInterval" => 30}
  end
end
