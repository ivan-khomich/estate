class Yandex::Campaign <  ActiveRecord::Base
  belongs_to :strategy, class_name: "Yandex::Strategy"
  belongs_to :email_notifications, class_name: "Yandex::EmailNotificationInfo"
  belongs_to :user
  has_many :banners, class_name: "Yandex::Banner"

  def create
    Yandex::API::Direct::request('CreateOrUpdateCampaign', self.to_hash)
  end

  def get_params
    Yandex::API::Direct::request('GetCampaignsParams', {"CampaignIDS" => [self.campaign_id]})
  end

  def to_moderate    
    Yandex::API::Direct::request('ModerateBanners', {"CampaignID" => self.campaign_id,
       "BannerIDS" => Array(self.banners.map{|b| b.banner_id})})
  end

  def to_hash
    {"CampaignID" => self.campaign_id,
     "Name" => self.name,
     "FIO"  => self.user.fio,
     # "StartDate" => self.start_date,
     "Strategy" => self.strategy.to_hash,
     "EmailNotification" => self.email_notifications.to_hash} 
  end
end
