class YandexController < ApplicationController
  def get_token
    require 'net/http'
    require 'net/https'
    require 'json'
    require 'uri'
    Yandex::API::Direct.load File.join(Rails.root,"config","yandex_direct.yml"), Rails.env

    url = URI.parse("https://oauth.yandex.ru/token")

    body = {
      :grant_type => "???",
      :code => params[:code],
      :client_id => Yandex::API::Direct.configuration['application_id'],
      :client_secret => Yandex::API::Direct.configuration['client_secret']
    }

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    response = http.post(url.path,JSON.generate(body))

    parsed = JSON.parse(response)

    render json: "ok"
  end

  def index
    Yandex::API::Direct.load File.join(Rails.root,"config","yandex_direct.yml"), Rails.env
    redirect_to "https://oauth.yandex.ru/authorize?response_type=code&client_id=#{Yandex::API::Direct.configuration['application_id']}"
  end
end