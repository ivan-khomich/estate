class Yandex::CampaignsController < ApplicationController
  before_action :set_yandex_campaign, only: [:show, :edit, :update, :destroy, :create_real, :mod_campaign]
  before_action :config_yandex, only: [:show, :create_real, :mod_campaign, :create]

  # GET /yandex/campaigns
  # GET /yandex/campaigns.json
  def index
    @yandex_campaigns = Yandex::Campaign.all
  end

  # GET /yandex/campaigns/1
  # GET /yandex/campaigns/1.json
  def show
    @status_mod = {"Yes" => "модератор одобрил хотя бы одно объявление",
        "No" => "модератор отклонил все объявления",
        "New" => "объявления не отправлялись на проверку (статус кампании «Черновик»)",
        "Pending" => "проводится проверка"}
    @params = @yandex_campaign.get_params.try(:first)
    @params = {} if @params.nil?
    p @params
  end

  # GET /yandex/campaigns/new
  def new
    @yandex_campaign = Yandex::Campaign.new
  end

  # GET /yandex/campaigns/1/edit
  def edit
  end

  # POST /yandex/campaigns
  # POST /yandex/campaigns.json
  def create
    @yandex_campaign = current_user.campaigns.build(name: params[:yandex_campaign][:name])
    @yandex_campaign.user = current_user
    @yandex_campaign.campaign_id = 0

    str = @yandex_campaign.build_strategy(name: params[:yandex_campaign][:strategy_name])
    str.campaign = @yandex_campaign
    str.save!

    email_n = @yandex_campaign.build_email_notifications
    email_n.campaign = @yandex_campaign
    email_n.save!

    id = @yandex_campaign.create
    @yandex_campaign.campaign_id = id
    @yandex_campaign.save

    respond_to do |format|
      if @yandex_campaign.save
        format.html { redirect_to @yandex_campaign, notice: 'Campaign was successfully created.' }
        format.json { render :show, status: :created, location: @yandex_campaign }
      else
        format.html { render :new }
        format.json { render json: @yandex_campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /yandex/campaigns/1
  # PATCH/PUT /yandex/campaigns/1.json
  def update
    respond_to do |format|
      if @yandex_campaign.update(yandex_campaign_params)
        format.html { redirect_to @yandex_campaign, notice: 'Campaign was successfully updated.' }
        format.json { render :show, status: :ok, location: @yandex_campaign }
      else
        format.html { render :edit }
        format.json { render json: @yandex_campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /yandex/campaigns/1
  # DELETE /yandex/campaigns/1.json
  def destroy
    @yandex_campaign.destroy
    respond_to do |format|
      format.html { redirect_to yandex_campaigns_url, notice: 'Campaign was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def create_real



    @yandex_campaign.banners.each do |b|
      id = b.create
      b.banner_id = id.first
      b.save!
    end

    respond_to do |format|
      if @yandex_campaign.save
        format.html { redirect_to @yandex_campaign, notice: 'Campaign was successfully updated.' }
        format.json { render :show, status: :ok, location: @yandex_campaign }
      else
        format.html { render :edit }
        format.json { render json: @yandex_campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  def mod_campaign
    id = @yandex_campaign.to_moderate

    redirect_to @yandex_campaign, notice: 'Кампания отправлена на модерацию.'
  end

  private
    def config_yandex
      Yandex::API::Direct.load File.join(Rails.root,"config","yandex_direct.yml"), Rails.env
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_yandex_campaign
      @yandex_campaign = Yandex::Campaign.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def yandex_campaign_params
      params.require(:yandex_campaign).permit(:name, :strategy_name)
    end
end
