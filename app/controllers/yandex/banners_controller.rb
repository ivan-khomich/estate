class Yandex::BannersController < ApplicationController
  before_action :set_yandex_banner, only: [:show, :edit, :update, :destroy]

  # GET /yandex/banners
  # GET /yandex/banners.json
  def index
    @yandex_banners = Yandex::Banner.all
  end

  # GET /yandex/banners/1
  # GET /yandex/banners/1.json
  def show
  end

  # GET /yandex/banners/new
  def new
    @yandex_banner = Yandex::Banner.new
  end

  # GET /yandex/banners/1/edit
  def edit
  end

  # POST /yandex/banners
  # POST /yandex/banners.json
  def create
    p params

    c = Yandex::Campaign.find params[:yandex_banner][:campaign_id]
    @yandex_banner = c.banners.build(campaign_id: params[:yandex_banner][:campaign_id], text: params[:yandex_banner][:text],
      title: params[:yandex_banner][:title], href: params[:yandex_banner][:href], geo: params[:yandex_banner][:geo], banner_id: 0)
    @yandex_banner.phrases = params[:yandex_banner][:phrases].split(';')


    respond_to do |format|
      if @yandex_banner.save
        format.html { redirect_to @yandex_banner.campaign, notice: 'Banner was successfully created.' }
        format.json { render :show, status: :created, location: @yandex_banner }
      else
        format.html { render :new }
        format.json { render json: @yandex_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /yandex/banners/1
  # PATCH/PUT /yandex/banners/1.json
  def update
    respond_to do |format|
      if @yandex_banner.update(yandex_banner_params)
        format.html { redirect_to @yandex_banner, notice: 'Banner was successfully updated.' }
        format.json { render :show, status: :ok, location: @yandex_banner }
      else
        format.html { render :edit }
        format.json { render json: @yandex_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /yandex/banners/1
  # DELETE /yandex/banners/1.json
  def destroy
    @yandex_banner.destroy
    respond_to do |format|
      format.html { redirect_to yandex_banners_url, notice: 'Banner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_yandex_banner
      @yandex_banner = Yandex::Banner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def yandex_banner_params
      params.require(:yandex_banner).permit(:banner_id, :campaign_id, :title, :text, :href, :geo, :phrases)
    end
end
