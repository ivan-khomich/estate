# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141002070047) do

  create_table "users", force: true do |t|
    t.string   "fio"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "role",                   default: 0
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "yandex_banners", force: true do |t|
    t.integer  "banner_id"
    t.integer  "campaign_id"
    t.text     "title"
    t.text     "text"
    t.string   "href"
    t.string   "geo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "phrases"
  end

  add_index "yandex_banners", ["campaign_id"], name: "index_yandex_banners_on_campaign_id", using: :btree

  create_table "yandex_campaigns", force: true do |t|
    t.integer  "strategy_id"
    t.integer  "email_notifications_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "campaign_id",            default: 0
    t.string   "name"
    t.integer  "user_id"
  end

  add_index "yandex_campaigns", ["email_notifications_id"], name: "index_yandex_campaigns_on_email_notifications_id", using: :btree
  add_index "yandex_campaigns", ["strategy_id"], name: "index_yandex_campaigns_on_strategy_id", using: :btree
  add_index "yandex_campaigns", ["user_id"], name: "index_yandex_campaigns_on_user_id", using: :btree

  create_table "yandex_email_notification_infos", force: true do |t|
    t.string   "email"
    t.string   "WarnPlaceInterval"
    t.string   "MoneyWarningValue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "campaign_id"
  end

  add_index "yandex_email_notification_infos", ["campaign_id"], name: "index_yandex_email_notification_infos_on_campaign_id", using: :btree

  create_table "yandex_strategies", force: true do |t|
    t.string   "name"
    t.string   "MaxPrice"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "campaign_id"
  end

  add_index "yandex_strategies", ["campaign_id"], name: "index_yandex_strategies_on_campaign_id", using: :btree

end
