class CreateYandexEmailNotificationInfos < ActiveRecord::Migration
  def change
    create_table :yandex_email_notification_infos do |t|
      t.string :email
      t.string :WarnPlaceInterval
      t.string :MoneyWarningValue

      t.timestamps
    end
  end
end
