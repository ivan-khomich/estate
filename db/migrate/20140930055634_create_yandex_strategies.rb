class CreateYandexStrategies < ActiveRecord::Migration
  def change
    create_table :yandex_strategies do |t|
      t.string :name
      t.string :MaxPrice

      t.timestamps
    end
  end
end
