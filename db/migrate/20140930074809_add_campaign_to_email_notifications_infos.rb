class AddCampaignToEmailNotificationsInfos < ActiveRecord::Migration
  def change
    add_reference :yandex_email_notification_infos, :campaign, index: true
  end
end
