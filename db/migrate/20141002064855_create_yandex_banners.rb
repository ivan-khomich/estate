class CreateYandexBanners < ActiveRecord::Migration
  def change
    create_table :yandex_banners do |t|
      t.integer :banner_id
      t.references :campaign, index: true
      t.text :title
      t.text :text
      t.string :href
      t.string :geo

      t.timestamps
    end
  end
end
