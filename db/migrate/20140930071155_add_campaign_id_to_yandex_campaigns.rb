class AddCampaignIdToYandexCampaigns < ActiveRecord::Migration
  def change
    add_column :yandex_campaigns, :campaign_id, :integer, default: 0
  end
end
