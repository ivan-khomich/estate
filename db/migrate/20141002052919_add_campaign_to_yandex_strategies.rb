class AddCampaignToYandexStrategies < ActiveRecord::Migration
  def change
    add_reference :yandex_strategies, :campaign, index: true
  end
end
