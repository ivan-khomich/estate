class AddUserToYandexCampaigns < ActiveRecord::Migration
  def change
    add_reference :yandex_campaigns, :user, index: true
  end
end
