class AddPhrasesToYandexBanners < ActiveRecord::Migration
  def change
    add_column :yandex_banners, :phrases, :text
  end
end
