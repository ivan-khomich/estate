class CreateYandexCampaigns < ActiveRecord::Migration
  def change
    create_table :yandex_campaigns do |t|
      t.references :strategy, index: true
      t.references :email_notifications, index: true

      t.timestamps
    end
  end
end
